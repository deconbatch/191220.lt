let p5js01_function = function(p) {
    const baseHue = 100.0;
    const randX = new Array();
    const randY = new Array();
    const currX = new Array();
    const currY = new Array();
    p.setup = function() {
	let myCanvas = p.createCanvas(1000, 600);
	myCanvas.parent('p5js01');
	p.colorMode(p.HSB,360,100,100, 100);
	p.rectMode(p.CENTER);
	p.noStroke();
	p.frameRate(10);
    }

    p.draw = function() {
	const lMax = 3;
	const radDiv = 0.1;
	
	p.background(0.0, 0.0, 100.0, 50.0);
	p.translate(0.0, p.height * 0.6);
	for (let lNum = 0; lNum < lMax; lNum++) {
	    let lRatio = p.map(lNum, 0, lMax, 0.0, 1.0);
	    for (let rad = 0; rad < p.TWO_PI; rad += radDiv) {
		let rRatio = p.map(rad, 0.0, p.TWO_PI, 0.0, 1.0);
		let waveShape = p.sin(rad * 2.0 + lRatio * 1.0 + p.frameCount * 0.2) * p.noise(lRatio, rRatio);
		
		let eX = p.width * rRatio;
		let eY = 200.0 * waveShape * p.sin(rad * 0.5);
		let eW = 3.0 + 5.0 * lRatio;
		let eH = 100.0 * p.noise(lRatio, p.frameCount * 0.1) * rRatio

		p.fill((baseHue + lRatio * 60.0  + (rad % 4) * 10.0 + p.frameCount * 0.5) % 360.0, 30.0, 80.0, 100.0);
		p.rect(eX, eY, eW, eH);
	    }
	}
    }
}
let f01 = new p5(p5js01_function)
