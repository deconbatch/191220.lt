let p5js02_function = function(p) {
    const baseHue = 200.0;
    let prevX = new Array();
    let prevY = new Array();
    let currX = new Array();
    let currY = new Array();
    p.setup = function() {
	let myCanvas = p.createCanvas(1000, 600);
	myCanvas.parent('p5js02');
	p.colorMode(p.HSB,360,100,100, 100);
	p.noStroke();
	p.frameRate(10);
	reset();
    }

    p.draw = function() {
	if (p.frameCount % 200 == 0) reset();
	p.background(0.0, 0.0, 100.0, 20.0);
	for (let i = 0; i < currX.length; i++) {
	    currX[i] += 0.005 * p.cos(p.TWO_PI * p.noise(p.frameCount * 0.0008, prevY[i]) * 8.0);
	    currY[i] += 0.005 * p.sin(p.TWO_PI * p.noise(p.frameCount * 0.0008, prevX[i]) * 8.0);
//	    currX[i] += 0.005 * p.cos(p.TWO_PI * p.noise(p.frameCount * 0.002, prevX[i], prevY[i]) * 12.0);
//	    currY[i] += 0.005 * p.sin(p.TWO_PI * p.noise(p.frameCount * 0.002, prevX[i], prevY[i]) * 12.0);
	    prevX[i] = currX[i];
	    prevY[i] = currY[i];
	    p.fill((baseHue + (i % 4) * 20.0 + p.frameCount * 0.8) % 360.0, 40.0, 80.0, 100.0);
	    p.ellipse(currX[i] * p.width, currY[i] * p.height, 5.0, 5.0);
	}
    }

    function reset() {
	prevX = [];
	prevY = [];
	currX = [];
	currY = [];
	for (let initX = 0.0; initX < 1.0; initX += 0.1) {
	    for (let initY = 0.0; initY < 1.0; initY += 0.1) {
		prevX.push(initX);
		prevY.push(initY);
		currX.push(initX);
		currY.push(initY);
	    }
	}
	p.background(0.0, 0.0, 100.0, 100.0);
    }
}
let f02 = new p5(p5js02_function)
