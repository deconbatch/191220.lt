let p5js03_function = function(p) {
    const baseHue = 200.0;
    const randX = new Array();
    const randY = new Array();
    const currX = new Array();
    const currY = new Array();
    p.setup = function() {
	let myCanvas = p.createCanvas(1000, 600);
	myCanvas.parent('p5js03');
	p.colorMode(p.HSB,360,100,100, 100);
	p.noFill();
	p.frameRate(10);
	reset();
    }

    p.draw = function() {
	p.background(0.0, 0.0, 100.0, 100.0);
	p.translate(p.width * 0.7, p.height * 0.55);
	for (let i = 0; i < currX.length; i++) {
	    let iRatio = p.map(i, 0, currX.length, 0.1, 1.0);
	    currX[i] = 280.0 * iRatio * p.cos((p.frameCount * 0.05 + 1.0) * randX[i]);
	    currY[i] = 280.0 * iRatio * p.sin((p.frameCount * 0.05 + 1.0) * randY[i]);
	}
	for (let i = 0; i < currX.length; i++) {
	    for (let j = i + 1; j < currX.length; j++) {
		let distance = p.dist(currX[i], currY[i], currX[j], currY[j]);
		if (distance < 120.0) {
		    p.strokeWeight(5.0);
		    p.stroke((baseHue + (i % 4) * 20.0 + p.frameCount * 0.1) % 360.0, 20.0, 80.0, 100.0 - distance * 0.8);
		    p.line(currX[i], currY[i], currX[j], currY[j]);
		}
	    }
	    p.fill((baseHue + (i % 4) * 20.0 + p.frameCount * 0.1) % 360.0, 40.0, 80.0, 100.0);
	    p.stroke(0.0, 0.0, 100.0, 100.0);
	    p.ellipse(currX[i], currY[i], 20.0, 20.0);
	}
    }

    function reset() {
	for (let i = 0; i < 10; i++) {
	    randX.push(p.random(0.4, 1.0));
	    randY.push(p.random(0.4, 1.0));
	    currX.push(0.0);
	    currY.push(0.0);
	}
	p.background(0.0, 0.0, 100.0, 100.0);
    }
}
let f03 = new p5(p5js03_function)
